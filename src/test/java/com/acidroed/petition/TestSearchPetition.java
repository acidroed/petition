package com.acidroed.petition;

import com.acidroed.petition.utility.PageLoadWaiter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**
 * Created by odeord on 17.11.2015.
 */
public class TestSearchPetition {

    protected String baseUrl = "https://petition.president.gov.ua";
    protected WebDriver driver;
    private String searchedText = "Metallica";
    private String expectedText = "Запросити в Україну легендарну рок-групу Metallica";
    private static final By SEARCH_FIELD = By.xpath("//input[@class='txt_input vat']");
    private static final By SEARCH_BUTTON = By.xpath("//input[@class='btn_input vat search']");
    private static final By TITLE_OF_COLUMN_IN_TABLE_OF_FOUND_RESULT =
            By.xpath("//div[@class='list_elem_col list_elem_col_head list_elem_title']");

    @BeforeSuite
    public void initTestSuite() {
        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void searchPetition(){
        driver.findElement(SEARCH_FIELD).sendKeys(searchedText);
        driver.findElement(SEARCH_BUTTON).click();
        //(new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(TITLE_OF_COLUMN_IN_TABLE_OF_FOUND_RESULT));
        PageLoadWaiter.waitForJStoLoad(driver);
        driver.findElement(TITLE_OF_COLUMN_IN_TABLE_OF_FOUND_RESULT);
        Assert.assertTrue(driver.getPageSource().contains(expectedText));
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }
}

