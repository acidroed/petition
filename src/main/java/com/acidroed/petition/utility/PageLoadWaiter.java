package com.acidroed.petition.utility;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by odeord on 18.11.2015.
 */
public class PageLoadWaiter {

    private static Object executeJavaScript(String jsExpression,  WebDriver driver){
        if (driver instanceof JavascriptExecutor) {
            return ((JavascriptExecutor) driver).executeScript(jsExpression);
        }
        return null;
    }


    public static boolean waitForJStoLoad(WebDriver driver) {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return (Long)(executeJavaScript("return jQuery.active", driver)) == 0;
                } catch (Exception e) {
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return executeJavaScript("return document.readyState", driver)
                            .toString().equals("complete");
                } catch (NullPointerException e) {
                    return false;
                }
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }
}
